﻿using GlobalGamesAspNet.Models;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GlobalGamesAspNet.Controllers
{
    public class HomeController : Controller
    {
        DataClasses1DataContext dc = new DataClasses1DataContext();

        public ActionResult Index()
        {
            return View();
        }
        
        public ActionResult Sobre()
        {
            return View();
        }


        public ActionResult Servicos()
        {
            return View();
        }

       
        [HttpPost]
        public ActionResult Servicos(Orcamento orcamento)
        {

            if (ModelState.IsValid)
            {
                dc.Orcamentos.InsertOnSubmit(orcamento);
            }

            try
            {
                dc.SubmitChanges();
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View();
            }
        }

        public ActionResult Newsletter()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Newsletter(Newsletter newsletter)
        {

            if (ModelState.IsValid)
            {
                dc.Newsletters.InsertOnSubmit(newsletter);
            }

            try
            {
                dc.SubmitChanges();
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View();
            }
        }

    }
}